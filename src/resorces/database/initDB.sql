CREATE TABLE IF NOT EXISTS developers (
  id         INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(20)  NOT NULL,
  last_name  VARCHAR(100) NOT NULL,
  specialty  VARCHAR(100) NOT NULL,
  experience INT          NOT NULL,
  project_id INT          NOT NULL,
  FOREIGN KEY (id) REFERENCES projects (id)

)
  ENGINE InnoDB;

CREATE TABLE IF NOT EXISTS skills (
  id   INT          NOT NULL  AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100) NOT NULL
)
  ENGINE InnoDB;

CREATE TABLE IF NOT EXISTS companies (
  id           INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name_company VARCHAR(200) NOT NULL

)
  ENGINE InnoDB;

CREATE TABLE IF NOT EXISTS customers (
  id            INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name_customer VARCHAR(100) NOT NULL
)
  ENGINE InnoDB;

CREATE TABLE IF NOT EXISTS developer_skills (
  developer_id INT NOT NULL,
  skill_id     INT NOT NULL,
  PRIMARY KEY (developer_id, skill_id),
  UNIQUE (developer_id, skill_id),
  FOREIGN KEY (developer_id) REFERENCES developers (id),
  FOREIGN KEY (skill_id) REFERENCES skills (id)
)
  ENGINE InnoDB;

CREATE TABLE IF NOT EXISTS projects (
  id           INT          NOT NULL  AUTO_INCREMENT PRIMARY KEY,
  name_project VARCHAR(100) NOT NULL,
  company_id   INT          NOT NULL,
  customer_id  INT          NOT NULL,
  UNIQUE (company_id,customer_id),
  FOREIGN KEY (company_id)REFERENCES companies(id),
  FOREIGN KEY (customer_id)REFERENCES customers(id)

)ENGINE InnoDB;

