INSERT INTO developers VALUES (1, 'Emmett', 'Brown', 'Java Developer', 1, 2, 1000);
INSERT INTO developers VALUES (2, 'Vasja', 'Popovich', 'C++ Developer', 9, 4, 1500);
INSERT INTO developers VALUES (3, 'Zina', 'Kupriyanovich', 'C# Developer', 5, 3, 1300);
INSERT INTO developers VALUES (4, 'Denis', 'Klaver', 'JavaScript Developer', 8, 1, 1450);
INSERT INTO developers VALUES (5, 'Ilja', 'Lagutenko', 'SQL Developer', 4, 3, 2000);
INSERT INTO developers VALUES (6, 'John', 'Cena', 'NoSQL Developer', 5, 2, 2100);

INSERT INTO skills VALUES (1, 'Java');
INSERT INTO skills VALUES (2, 'C++');
INSERT INTO skills VALUES (3, 'SQL');
INSERT INTO skills VALUES (4, 'STL');
INSERT INTO skills VALUES (5, 'NoSQL');
INSERT INTO skills VALUES (6, 'C#');
INSERT INTO skills VALUES (7, 'JavaScript');

INSERT INTO customers VALUES (1, 'Alan');
INSERT INTO customers VALUES (2, 'Kelvin');
INSERT INTO customers VALUES (3, 'Robert');
INSERT INTO customers VALUES (4, 'Nancy');
INSERT INTO customers VALUES (5, 'Julia');

INSERT INTO companies VALUES (1, 'Google');
INSERT INTO companies VALUES (2, 'Microsoft');
INSERT INTO companies VALUES (3, 'Apple');
INSERT INTO companies VALUES (4, 'Tesla motors');
INSERT INTO companies VALUES (5, 'Qualcomm');

INSERT INTO projects VALUES (1, 'Privet bank system project', 2, 1);
INSERT INTO projects VALUES (2, 'Universal bank system project', 1, 2);
INSERT INTO projects VALUES (3, 'Alpha bank system project', 4, 5);
INSERT INTO projects VALUES (4, 'Pumb bank system project', 3, 4);
INSERT INTO projects VALUES (5, 'Raiffeisen bank system project', 4, 3);
INSERT INTO projects VALUES (6, 'ProCredit bank system project', 5, 4);
INSERT INTO projects VALUES (7, 'Ohshit bank system project', 1, 2);

INSERT INTO developer_skills VALUES (1, 1);
INSERT INTO developer_skills VALUES (2, 2);
INSERT INTO developer_skills VALUES (3, 5);
INSERT INTO developer_skills VALUES (4, 7);
INSERT INTO developer_skills VALUES (5, 3);
INSERT INTO developer_skills VALUES (6, 4);










